#!/bin/bash

C_CONF_BASE="/upstream/vagrantenv/conf"
C_PACKAGES="dhcp syslinux-tftpboot tftp-server httpd"
C_SERVICES="dhcpd xinetd iptables httpd"

echo "Install packages..."
yum install -y ${C_PACKAGES} &> /dev/null

echo "Install configuration files..."
cp ${C_CONF_BASE}/dhcpd.conf /etc/dhcp/dhcpd.conf
cp ${C_CONF_BASE}/dhcpd_sysconfig /etc/sysconfig/dhcpd
cp ${C_CONF_BASE}/tftp_xinetd /etc/xinetd.d/tftp
cp ${C_CONF_BASE}/httpd.conf /etc/httpd/conf/httpd.conf
cp ${C_CONF_BASE}/sysctl.conf /etc/sysctl.conf
cp ${C_CONF_BASE}/iptables /etc/sysconfig/iptables

echo "Configuring kernel parameters..."
sysctl -p

echo "Configuring permissions..."
chmod -R +rx /upstream/tftpboot/

echo "Configuring services..."
for myservice in ${C_SERVICES}; do 
  chkconfig $myservice on
  service $myservice restart
done

echo "Done!"

exit 0



