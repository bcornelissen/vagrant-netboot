# Vagrant-netboot
Quickly builds a Vagrant environment with a CentOS 6 virtual machine for the purpose of netinstalling. It provides DHCP, TFTP, HTTPD and IP Masquerading (for internet access) at the moment.

## Networking
The VM will have 2 network interfaces. The primary interface, `eth0`, will be a NAT interface connected to the primary network interface of your workstation (I use WiFi). The second interface, `eth1`, will be a bridged interface, connected to a secondary interface. I provided a default in the Vagrantfile but if that interface is not found on your system, Vagrant will ask you to choose the appropriate interface. The eth1 interface has static IP configuration: `10.9.8.1/24`. 

### DHCP server
The DHCP server will run *only* on `eth1`, and will use the `10.9.8.0/24` subnet. It will use Google DNS servers, and serve out `pxelinux.0` as the default file (for netbooting).

*WARNING*: this VM runs a DHCP server on a bridged interface. DO NOT hook up your workstation to the local network unless you intend to expose this DHCP server to your local network. This setup is intended for use with a direct connection to a server that needs to be netbooted, or an isolated network without DHCP server. 

### TFTP server
The TFTP server will use `/upstream/tftpboot` as its root path. This allows you to store your netboot content (kernels, initrd files, ISOs) and PXE configuration outside of the VM, reducing hassle. Please note however, that all files in the root path need to be world-readable, otherwise booting might not work.

*note*: the contents of `tftpboot/data` are excluded from Git. You can safely store your ISOs and install content there. 

## PXE configuration
The PXE boot menu can be configured in `tftpboot/pxelinux.cfg/default`. It already contains a working setup for booting the CentOS 6 and XenServer 6.5 installers and sample config for using the aforementioned `tftpboot/data` directory for other OSes you want to add later. 

### Quick 'n dirty ISO booting
Sometimes you don't have the time to properly prepare a NetBoot setup, and you just need to boot a server from an ISO image you would have otherwised burned on a DVD+R. You can easily boot from an iso using this method in `pxelinux.cfg/default`: 

```
label myOS
menu label Install myOS from ISO
kernel memdisk
append initrd=data/myOS/myOS.iso iso
```

## Using the HTTPD
I have set the `DocumentRoot` to be `/upstream/tftpboot` and allowed Indexes globally so everything inside that directory is readable. Note that this is *not* a secure setup, but in this case that is not really a concern. 

### Using the CentOS install with local data
1. Download the CentOS DVD1
1. Mount the DVD
1. Copy the entire contents to `tftpboot/data/centos6`
1. Modify the `pxelinux.cfg/default` so that the line starting with 'append' is as follows: `append initrd=centos6/x86_64/initrd.img method=http://10.9.8.1/data/centos6 ip=dhcp`
1. You can now use your local data

## Future functionality
...